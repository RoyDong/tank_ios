//
//  BattleController.h
//  tank
//
//  Created by Roy on 8/4/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "IndexController.h"

@interface BattleController : UIViewController <UIAlertViewDelegate>

@property (nonatomic) IndexController *index;

@property (nonatomic) IBOutlet UIButton *battle;

@property (nonatomic) IBOutlet UISegmentedControl *battleType;

@property (nonatomic) IBOutlet UILabel *name;

@property (nonatomic) IBOutlet UILabel *rank;

- (IBAction)findBattle:(id)sender;

- (IBAction)signout:(id)sender;

@end
