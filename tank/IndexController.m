//
//  IndexController.m
//  tank
//
//  Created by Roy on 7/31/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "IndexController.h"
#import "BattleController.h"
#import "SigninController.h"


@implementation IndexController


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)popMessage:(NSString *)text
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:text
                                                   message:nil
                                                  delegate:self
                                         cancelButtonTitle:@"ok"
                                         otherButtonTitles:nil, nil];
    
    [alert show];
}


- (void)viewDidAppear:(BOOL)animated
{
    TcpClient *client = [TcpClient singleInstance];
    
    if (client.status == 0)
    {
        client = [TcpClient singleInstance];
        client.ip = @"121.199.35.219";
        //client.ip = @"192.168.11.177";
        //client.ip = @"192.168.10.192";
        client.port = 3721;
        
        [client connect];
    }
    
    User *user = [User current];
    if (user == nil)
    {
        SigninController *c = [[SigninController alloc] init];
        c.index = self;
        [self presentViewController:c animated:true completion:nil];
    }
}



@end