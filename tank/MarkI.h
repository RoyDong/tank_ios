//
//  MarkI.h
//  tank
//
//  Created by Roy on 8/18/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Cannon.h"

@interface MarkI : Sprite

@property (nonatomic) GLKVector2 direction;

@property (nonatomic) float speed;

@property (nonatomic) int status;

@property (nonatomic, strong) Cannon *cannon;


- (void)fireAtPosition:(GLKVector2)position;


@end
