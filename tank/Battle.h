//
//  Battle.h
//  tank
//
//  Created by Roy on 8/6/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//


#import "Scene.h"
#import "User.h"

@interface Battle : NSObject

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong, readonly) User *creator;

@property (nonatomic, strong, readonly) Scene *scene;


+ (Battle *)singleInstance;

+ (void)setInstance:(Battle *)battle;

- (Battle *)initWithDict:(NSDictionary *)dict;

- (Sprite *)addSpriteWithData:(NSDictionary *)data;

- (Sprite *)sprite:(int)sid;

- (void)syncSceneWithData:(NSDictionary *)data;

- (void)addPlayer:(User *)player;


@end