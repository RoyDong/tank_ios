//
//  AppDelegate.m
//  tank
//
//  Created by Roy on 7/31/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "AppDelegate.h"
#import "IndexController.h"
#import "TcpClient.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [[IndexController alloc] initWithNibName:@"IndexController" bundle:nil];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}



@end