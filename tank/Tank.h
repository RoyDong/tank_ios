//
//  Tank.h
//  sgg
//
//  Created by Roy on 7/25/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Cannon.h"

@interface Tank : Sprite


@property (nonatomic) GLKVector2 destPosition;

@property (nonatomic) float speed;

@property (nonatomic) int status;

@property (nonatomic, strong) Cannon *cannon;


- (void)fireAtPosition:(GLKVector2)position;

@end
