//
//  IndexController.h
//  low_ios
//
//  Created by Roy on 7/30/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "IndexController.h"
#import "MarkI.h"

@interface SceneController : GLKViewController <UIAlertViewDelegate>

@property (strong, nonatomic) EAGLContext *context;

@property (nonatomic, weak) IndexController *index;

@property (nonatomic, strong) MarkI *tank;

@property (nonatomic, strong) NSDictionary *battle;


@end
