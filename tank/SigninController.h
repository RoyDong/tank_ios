//
//  SigninController.h
//  tank
//
//  Created by Roy on 8/24/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "IndexController.h"

@interface SigninController : UIViewController

@property (nonatomic) IndexController *index;

@property (nonatomic, strong) UIButton *signinBtn;

@property (nonatomic, strong) UIButton *signupBtn;

@property (nonatomic, strong) UIButton *forgetPassBtn;

@property (nonatomic, strong) UITextField *emailInput;

@property (nonatomic, strong) UITextField *passInput;

@end
