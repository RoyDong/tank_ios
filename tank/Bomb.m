//
//  Bomb.m
//  sgg
//
//  Created by Roy on 7/25/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Bomb.h"

@implementation Bomb
{
    float timeSinceFire;
}

@synthesize launcher = _launcher;

@synthesize speed = _speed;

@synthesize timeout = _timeout;


- (void)flyByAngle:(float)angle
{
    self.velocity = GLKVector2Make(_speed * cosf(angle), _speed * sinf(angle));
}


- (void)update:(float)dt
{
    timeSinceFire += dt;

    if (timeSinceFire > _timeout)
    {
        [self destroy];
    }
    else
        [super update:dt];
}

@end