//
//  Bomb.h
//  sgg
//
//  Created by Roy on 7/25/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Sprite.h"

@class Cannon;

@interface Bomb : Sprite

@property (nonatomic, weak) Cannon *launcher;

@property (nonatomic) float speed;

@property (nonatomic) float timeout;

- (void)flyByAngle:(float)angle;

@end