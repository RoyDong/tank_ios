//
//  BattleController.m
//  tank
//
//  Created by Roy on 8/4/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "BattleController.h"
#import "TcpClient.h"
#import "SceneController.h"

@interface BattleController ()

@end

@implementation BattleController
{
    UIActivityIndicatorView *spinner;
    TcpClient *client;
    User *user;
}


- (void)viewDidLoad
{
    user = [User current];
    client = [TcpClient singleInstance];
    [super viewDidLoad];
    self.name.text = [NSString stringWithFormat:@"Name: %@", user.name];
    self.rank.text = [NSString stringWithFormat:@"Level: %i", user.rank];
    
    
    [client callWithTitle:@"user.Current" content:nil callback:^(NSDictionary *dict){
        if ([[dict objectForKey:@"code"] intValue] == 0)
        {
            user.rank = [[[dict objectForKey:@"data"] objectForKey:@"rank"] intValue];
            self.name.text = [NSString stringWithFormat:@"Name: %@", user.name];
            self.rank.text = [NSString stringWithFormat:@"Level: %i", user.rank];
        }
        else
        {
            user = nil;
            [User setCurrent:nil];
            [self.index dismissViewControllerAnimated:false completion:nil];
        }
    }];
    
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:spinner];

    __weak UIButton *__b = self.battle;
    __weak BattleController *__c = self;
    __weak UIActivityIndicatorView *__s = spinner;
    __weak User *__user = user;

    [client setHandler:^(NSDictionary *notice){
        [__b setTitle:@"战斗开始" forState:UIControlStateNormal];
        [__c enterScene];
    } forNotice:@"battle_begins"];

    [client setHandler:^(NSDictionary *notice){
        NSArray *d = (NSArray *)notice;
        __user.tag = [[d objectAtIndex:1] intValue];
        
        [__b setTitle:@"匹配成功" forState:UIControlStateNormal];
        [__s stopAnimating];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"匹配成功"
                                                       message:@"就绪确认"
                                                      delegate:__c
                                             cancelButtonTitle:@"取消"
                                             otherButtonTitles:@"确认", nil];
        [alert show];
    } forNotice:@"battle_ready"];
    
    [client setHandler:^(NSDictionary *notice){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"游戏结束"
                                                       message:@"有玩家取消了游戏"
                                                      delegate:__c
                                             cancelButtonTitle:@"确定"
                                             otherButtonTitles:nil, nil];
        [alert show];
        [__b setTitle:@"开始战斗" forState:UIControlStateNormal];
    } forNotice:@"battle_cancled"];
    
    [client setHandler:^(NSDictionary *notice){
        __user.tid = [((id)notice) intValue];
    } forNotice:@"user_tank"];

    if (user.state == UserStateInBattle)
    {
        [self.battle setTitle:@"回到战斗" forState:UIControlStateNormal];
    }
    else if (user.state == UserStateMatching)
    {
        [self.battle setTitle:@"匹配中..." forState:UIControlStateNormal];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [client callWithTitle:@"battle.Cancle" content:nil callback:nil];
    }
    else if (buttonIndex == 1)
    {
        [self.battle setTitle:@"等待其他玩家" forState:UIControlStateNormal];
        [client callWithTitle:@"battle.Ready" content:nil callback:nil];
    }
}


- (IBAction)findBattle:(id)sender
{
    [self.battle setEnabled:false];
    [self.battle setTitle:@"匹配中..." forState:UIControlStateNormal];
    CGSize size = self.view.bounds.size;
    [spinner setCenter:CGPointMake(size.width / 2, size.height / 2)];
    [spinner startAnimating];
    [client callWithTitle:@"battle.Match"
                  content:[NSString stringWithFormat:@"type=%i&test=1", self.battleType.selectedSegmentIndex + 1]
                 callback:^(NSDictionary *dict){
        if ([[dict objectForKey:@"code"] intValue] > 0)
        {
            [self.battle setTitle:@"开始战斗" forState:UIControlStateNormal];
            [self.battle setEnabled:true];
            [spinner stopAnimating];
            [self.index popMessage:@"无法加入搜索队列，请重试"];
        }
    }];
}

- (IBAction)signout:(id)sender
{
    user = nil;
    [User setCurrent:nil];
    [self.index dismissViewControllerAnimated:false completion:nil];
}


- (void)enterScene
{
    SceneController *c = [[SceneController alloc] init];
    c.index = self.index;
    [c.index dismissViewControllerAnimated:NO completion:nil];
    [c.index presentViewController:c animated:NO completion:nil];
}


@end
