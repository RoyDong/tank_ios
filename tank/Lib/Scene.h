//
//  Scene.h
//  sgg
//
//  Created by Roy on 7/22/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//



#import "Sprite.h"

@interface Scene : NSObject

@property (nonatomic) int sid;

@property (strong, nonatomic, readonly) EAGLContext *context;

@property (strong, nonatomic, readonly) GLKBaseEffect *effect;

@property (nonatomic) CGRect bounds;

+ (Scene *)singleInstance;

+ (void)setInstance:(Scene *)scene;

- (void)setView:(GLKView *)view;

- (void)addSprite:(Sprite *)sprite;

- (Sprite *)sprite:(int)sid;

- (void)removeSprite:(Sprite *)sprite;

- (void)perform:(float)dt;

- (void)show;

- (void)moveCameraByVector:(CGPoint)vector;

@end