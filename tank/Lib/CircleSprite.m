//
//  CircleSprite.m
//  tank
//
//  Created by Roy on 8/15/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "CircleSprite.h"

@implementation CircleSprite


- (void)render:(GLKBaseEffect *)effect
{
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


    

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableVertexAttribArray(GLKVertexAttribPosition);
    glDisableVertexAttribArray(GLKVertexAttribTexCoord0);


    glDisable(GL_BLEND);
}

@end
