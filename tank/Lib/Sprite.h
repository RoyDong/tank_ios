//
//  Animation.h
//  sgg
//
//  Created by Roy on 7/19/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@class Scene;

#define CollisionTypeNone 0
#define CollisionTypeRect 1
#define CollisionTypeCircle 2

typedef struct {
    GLfloat x;
    GLfloat y;
} Vertex2D;

Vertex2D Vertex2DMake(GLfloat x, GLfloat y);

bool CircleIntersectsRect(GLKVector2 center, float radius, CGRect rect);

@interface Sprite : NSObject

@property (nonatomic) int sid;

@property (nonatomic, strong) NSString *image;

@property (strong, nonatomic, readonly) GLKTextureInfo *textureInfo;

@property (nonatomic) float radius;

@property (nonatomic) GLKVector2 position;

@property (nonatomic) float rotation;

@property (nonatomic) int collisionType;

@property (nonatomic, strong, readonly) NSMutableSet *intersectors;

@property (nonatomic) GLKVector2 velocity;

@property (nonatomic) float rotateSpeed;

@property (nonatomic, strong, readonly) NSMutableDictionary *events;

@property (nonatomic) BOOL display;

@property (nonatomic) int z;

@property (nonatomic, weak) Scene *scene;

@property (nonatomic) int tag;

@property (nonatomic) BOOL visible;


- (void)loadImage;

- (void)initPropertiesWithDict:(NSDictionary *)dict;

- (void)update:(float)dt;

- (void)render:(GLKBaseEffect *)effect;

- (BOOL)isAtPosition:(GLKVector2)position;

- (BOOL)isIntersectsWithSprite:(Sprite *)sprite;

- (void)addIntersector:(Sprite *)sprite;

- (void)clearIntersectors;

- (void)destroy;

- (CGRect)rect;

- (void)addHandler:(void (^)())handler onEvent:(NSString *)name;

- (void)clearHandlersOnEvent:(NSString *)name;

- (void)triggerEvent:(NSString *)name;

@end