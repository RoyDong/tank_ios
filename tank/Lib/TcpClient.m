//
//  TcpClient.m
//  Recite
//
//  Created by Roy on 7/12/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "TcpClient.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <netdb.h>
#import <arpa/inet.h>


const int MaxCall = 20;

const int MaxMillisec = 10000;

const int NoticeMid = 63;

const int CheckInterval = 5;

const int MaxTitleLen = 1000;

@implementation TcpClient
{
    int sockeId;
    struct sockaddr_in socketParams;
    NSMutableArray *callbacks;
    uint64_t calltimes[MaxCall];
    NSThread *listenLoop;
    NSThread *checkLoop;
}

@synthesize host = _host;

@synthesize ip = _ip;

@synthesize port = _port;

@synthesize code = _code;

@synthesize message = _message;

@synthesize status = _status;

@synthesize notices = _notices;

static TcpClient *singleInstance;


+ (TcpClient *)singleInstance
{
    if(!singleInstance)
    {
        singleInstance = [[TcpClient alloc] init];
    }

    return singleInstance;
}

- (TcpClient *)init
{
    self = [super init];
    
    if (self)
    {
        _notices = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (void)setHost:(NSString *)host
{
    struct hostent *hostEnt = gethostbyname([host UTF8String]);
    
    if (hostEnt)
    {
        socketParams.sin_family = AF_INET;
        bcopy(hostEnt->h_addr_list[0], &socketParams.sin_addr, hostEnt->h_length);
        _host = host;
        _status = 0;
    }
    else
    {
        _message = @"could not get host info";
    }
}

- (void)setIp:(NSString *)ip
{
    socketParams.sin_addr.s_addr = inet_addr([ip UTF8String]);
    _ip = ip;
    _status = 0;
}

- (void)setPort:(int)port
{
    socketParams.sin_port = htons(port);
    _status = 0;
    _port = port;
}

- (BOOL)connect
{
    sockeId = socket(AF_INET, SOCK_STREAM, 0);

    if (sockeId == -1)
    {
        _message = @"failed to create socket";
        close(sockeId);
        return false;
    }
    
    int ret = connect(sockeId, (struct sockaddr *)&socketParams, sizeof(socketParams));
    
    if (ret == -1)
    {
        _message = [NSString stringWithFormat:@"connect to %@:%i failed", _host ? _host : _ip, _port];
        close(sockeId);
        return false;
    }

    callbacks = [NSMutableArray arrayWithCapacity:MaxCall];
    for (int i = 0; i < MaxCall; i++) calltimes[i] = 0;
    listenLoop = [[NSThread alloc] initWithTarget:self selector:@selector(listen) object:nil];
    [listenLoop setName:@"tcpListenLoop"];
    checkLoop = [[NSThread alloc] initWithTarget:self selector:@selector(check) object:nil];
    [checkLoop setName:@"callbackCheckLoop"];
    _status = 1;
    [listenLoop start];
    [checkLoop start];
    return true;
}

- (void)check
{
    while (_status)
    {
        NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
        int64_t t = [[NSNumber numberWithDouble:time * 1000] longLongValue];
        for (int i = 0; i < MaxCall; i++)
        {
            if (calltimes[i] > 0 && calltimes[i] < t - MaxMillisec) {
                void (^callback)(NSDictionary *reply) = [callbacks objectAtIndex:i];
                calltimes[i] = 0;
                if (callback)
                {
                    NSDictionary *result = [[NSDictionary alloc] initWithObjectsAndKeys:
                                            @"1", @"code",
                                            @"Network busy", @"message",
                                            @"null", @"data", nil];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{callback(result);}];
                }
            }
        }
        [NSThread sleepForTimeInterval:CheckInterval];
    }
    
    [NSThread exit];
}

- (void)listen
{
    while (_status)
    {
        char head[17];
        int result = recv(sockeId, head, 17, 0);

        if (result == 17)
        {
            int64_t sentTime;
            int titleLen, contentLen, mid = head[0];
            bcopy(head + 1, (void *)&sentTime, 8);
            bcopy(head + 9, (void *)&titleLen, 4);
            bcopy(head + 13, (void *)&contentLen, 4);
            if (titleLen > MaxTitleLen) continue;
            char titleBuffer[titleLen];
            result = recv(sockeId, titleBuffer, titleLen, 0);
            if (result != titleLen) continue;
            char contentBuffer[contentLen];
            result = 0;
            while (result < contentLen)
            {
                int l = recv(sockeId, contentBuffer + result, contentLen - result, 0);
                if (l == 0) break;
                result += l;
            }
            if (result != contentLen) continue;

            NSString *title = [[NSString alloc] initWithBytes:titleBuffer length:titleLen encoding:NSUTF8StringEncoding];
            NSString *c = [[NSString alloc] initWithBytes:contentBuffer length:contentLen encoding:NSUTF8StringEncoding];
            NSLog(@"%i %@ %@", mid, title, c);
            c = nil;
            
            NSData *content = [[NSData alloc] initWithBytes:contentBuffer length:contentLen];
            NSError *error;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:content options:NSJSONReadingAllowFragments error:&error];
            if (error != nil)
            {
                _message = @"data is not json";
                NSLog(@"data is not json");
                continue;
            }
            content = nil;

            if (mid == NoticeMid)
            {
                NSMutableSet *handlers = [_notices objectForKey:title];
                    
                if (handlers)
                {
                    for (void (^handler)(NSDictionary *c) in handlers)
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{handler(result);}];
                }
            }
            else if (mid >= 0 && mid < MaxCall)
            {
                int64_t callTime = calltimes[mid];
                NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
                int64_t t = [[NSNumber numberWithDouble:time * 1000] longLongValue];
                void (^callback)(NSDictionary *reply) = [callbacks objectAtIndex:mid];
                if (callTime > 0 && t - callTime <= MaxMillisec && callback)
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{callback(result);}];
                calltimes[mid] = 0;
            }
        }
        else if (result == 0)
        {
            _message = @"closed by server";
            break;
        }
    }

    NSLog(@"%@", _message);
    [self close];
    [NSThread exit];
}

- (void)close
{
    _status = 0;
    close(sockeId);
}

- (BOOL)callWithTitle:(NSString *)title content:(NSString *)content callback:(void (^)(NSDictionary *reply))callback
{
    if (!title || title.length > MaxTitleLen) return false;

    if (_status == 0 && [self connect] == false && callback)
    {
        callback([[NSDictionary alloc] initWithObjectsAndKeys:
                  @"1", @"code",
                  @"Network busy", @"message",
                  @"null", @"data", nil]);
        return false;
    }

    int mid;
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    int64_t t = [[NSNumber numberWithDouble:time * 1000] longLongValue];

    if (callback == nil)
    {
        mid = NoticeMid - 1;
    }
    else
    {
        for (mid = 0; mid < MaxCall; mid++)
        {
            [callbacks setObject:callback atIndexedSubscript:mid];
            calltimes[mid] = t;
            break;
        }

        if (mid >= MaxCall)
        {
            _message = @"Network is busy";
            return NO;
        }
    }

    if (content == nil) content = @"";
    const char *titleBuf = [title UTF8String];
    const char *contentBuf = [content UTF8String];
    int titleLen = strlen(titleBuf);
    int contentLen = strlen(contentBuf);
    int length = 17 + titleLen + contentLen;

    char buffer[length];
    buffer[0] = mid;
    bcopy((void *)&t, buffer + 1, 8);
    bcopy((void *)&titleLen, buffer + 9, 4);
    bcopy((void *)&contentLen, buffer + 13, 4);
    bcopy(titleBuf, buffer + 17, titleLen);
    bcopy(contentBuf, buffer + 17 + titleLen, contentLen);

    return send(sockeId, buffer, length, AF_INET) == length;
}

- (NSString *)buildContent:(NSDictionary *)parameters
{
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    NSString *value;
    
    for(NSString *key in parameters)
    {
        value = [parameters objectForKey:key];
        [parts addObject:[NSString stringWithFormat:@"%@=%@", key, value]];
    }
    
    return [parts componentsJoinedByString:@"&"];
}


- (void)setHandler:(void (^)(NSDictionary *content))handler forNotice:(NSString *)title
{
    NSMutableSet *handlers = [_notices objectForKey:title];
    handlers = [[NSMutableSet alloc] init];
    [_notices setObject:handlers forKey:title];
    [handlers addObject:handler];
}

- (void)addHandler:(void (^)(NSDictionary *content))handler forNotice:(NSString *)title
{
    NSMutableSet *handlers = [_notices objectForKey:title];
    
    if (handlers == nil)
    {
        handlers = [[NSMutableSet alloc] init];
        [_notices setObject:handlers forKey:title];
    }

    [handlers addObject:handler];
}

- (void)clearHandlersForNotice:(NSString *)title
{
    [_notices removeObjectForKey:title];
}


@end