//
//  Vehicle.h
//  sgg
//
//  Created by Roy on 7/22/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Sprite.h"

@interface Move : Sprite

@property (nonatomic) GLKVector2 destPosition;

@property (nonatomic) float speed;

@property (nonatomic) int status;

@end
