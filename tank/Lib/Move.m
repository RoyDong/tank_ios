//
//  Vehicle.m
//  sgg
//
//  Created by Roy on 7/22/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Move.h"

@implementation Move
{
    float destRadian;
}

@synthesize destPosition = _destPosition;

@synthesize speed = _speed;

@synthesize status = _status;

- (void)initPropertiesWithDict:(NSDictionary *)dict
{
    [super initPropertiesWithDict:dict];

    _speed = [[dict objectForKey:@"speed"] floatValue];
    NSDictionary *v = [dict objectForKey:@"dest_position"];
    _destPosition = GLKVector2Make([[v objectForKey:@"x"] floatValue], [[v objectForKey:@"y"] floatValue]);
}

- (void)setDestPosition:(GLKVector2)position
{
    if ([self isAtPosition:position]) return;
    _destPosition = position;
    
    if (self.rotateSpeed >= 0)
    {
        float w = _destPosition.x - self.position.x;
        float h = _destPosition.y - self.position.y;
        float s = sqrtf(w * w + h * h);
        destRadian = acosf(w / s);
        if (h < 0) destRadian = 2 * M_PI - destRadian;
        if (self.rotateSpeed == 0) self.rotation = destRadian;
    }
    
    _status = 1;
}

- (void)update:(float)dt
{    
    if (_status == 0 || _speed == 0) return;

    float w = _destPosition.x - self.position.x;
    float h = _destPosition.y - self.position.y;
    float s = sqrtf(powf(w, 2) + powf(h, 2));
    float ds = _speed * dt;

    if (s <= ds)
    {
        self.position = _destPosition;
        _status = 0;
        [self triggerEvent:@"arrive"];
    }
    else
    {
        float cos = w / s;

        if (self.rotateSpeed > 0 && self.rotation != destRadian)
        {
            float r = destRadian - self.rotation;
            float dr = self.rotateSpeed * dt;
            
            if (r < - M_PI)
            {
                self.rotation += dr;
                r = 2 * M_PI + r;
            }
            else if (r < 0)
            {
                self.rotation -= dr;
                r = -r;
            }
            else if (r <= M_PI)
            {
                self.rotation += dr;
            }
            else
            {
                r = 2 * M_PI - r;
                self.rotation -= dr;
            }
            
            if (dr > r) self.rotation = destRadian;
        }
        else
            self.position = GLKVector2Make(self.position.x + ds * cos, self.position.y + ds * h / s);
    }
}

@end
