//
//  TcpClient.h
//  Recite
//
//  Created by Roy on 7/12/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface TcpClient : NSObject

@property (nonatomic, strong) NSString *host;

@property (nonatomic, strong) NSString *ip;

@property (nonatomic, readonly) int code;

@property (nonatomic, readonly, strong) NSString *message;

@property (nonatomic) int status;

@property (nonatomic) int port;

@property (nonatomic, strong, readonly) NSMutableDictionary *notices;

+ (TcpClient *)singleInstance;

- (BOOL)connect;

- (void)close;

- (NSString *)buildContent:(NSDictionary *)parameters;

- (BOOL)callWithTitle:(NSString *)title content:(NSString *)content callback:(void (^)(NSDictionary *reply))callback;

- (void)setHandler:(void (^)(NSDictionary *content))handler forNotice:(NSString *)title;

- (void)addHandler:(void (^)(NSDictionary *content))handler forNotice:(NSString *)title;

- (void)clearHandlersForNotice:(NSString *)title;

@end
