//
//  Scene.m
//  sgg
//
//  Created by Roy on 7/22/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Scene.h"


@implementation Scene
{
    NSMutableDictionary *idIndex;
    NSMutableArray *depthIndex;
    NSMutableSet *sprites;
    NSMutableArray *displays;
}

static Scene * instance;

const int SceneDepth = 5;

@synthesize context = _context;

@synthesize effect = _effect;

@synthesize bounds = _bounds;

+ (Scene *)singleInstance
{
    return instance;
}

+ (void)setInstance:(Scene *)scene
{
    instance = scene;
}

- (Scene *)init
{
    self = [super init];
    
    if (self)
    {
        sprites = [[NSMutableSet alloc] init];
        displays = [[NSMutableArray alloc] init];
        depthIndex = [[NSMutableArray alloc] initWithCapacity:SceneDepth];
        idIndex = [[NSMutableDictionary alloc] init];

        for (int z = 0; z < SceneDepth; z++)
            [depthIndex addObject:[[NSMutableSet alloc] init]];
    }
    
    return self;
}

- (void)clearDepth
{
    for (int z = 0; z < SceneDepth; z++)
        [[depthIndex objectAtIndex:z] removeAllObjects];
}

- (void)setView:(GLKView *)view
{
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if (!_context)
    {
        NSLog(@"failed to create es context");
        return;
    }

    view.context = _context;
    [EAGLContext setCurrentContext:_context];
    _effect = [[GLKBaseEffect alloc] init];
    _bounds = view.bounds;

    GLKMatrix4 projectionMatrix = GLKMatrix4MakeOrtho(_bounds.origin.x,
                                                      _bounds.size.width,
                                                      _bounds.origin.y,
                                                      _bounds.size.height, -100, 100);
    _effect.transform.projectionMatrix = projectionMatrix;
}

- (void)moveCameraByVector:(CGPoint)vector
{
    CGPoint origin = _bounds.origin;
    _bounds.origin = CGPointMake(origin.x + vector.x, origin.y + vector.y);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakeOrtho(_bounds.origin.x,
                                                      _bounds.origin.x + _bounds.size.width,
                                                      _bounds.origin.y,
                                                      _bounds.origin.y + _bounds.size.height, -100, 100);
    _effect.transform.projectionMatrix = projectionMatrix;
}

- (void)addSprite:(Sprite *)sprite
{
    NSNumber *sid = [NSNumber numberWithInt:sprite.sid];
    if ([idIndex objectForKey:sid] == nil)
    {
        sprite.scene = self;
        [sprites addObject:sprite];
        if (sprite.sid > 0) [idIndex setObject:sprite forKey:sid];
    }
}

- (Sprite *)sprite:(int)sid
{
    return [idIndex objectForKey:[NSNumber numberWithInt:sid]];
}

- (void)removeSprite:(Sprite *)sprite
{
    [sprites removeObject:sprite];
    if (sprite.sid) [idIndex removeObjectForKey:[NSNumber numberWithInt:sprite.sid]];
}

- (void)perform:(float)dt
{
    Sprite *s;
    
    for (s in displays) [s update:dt];

    for (int i = 0; i < displays.count; i++)
    {
        s = [displays objectAtIndex:i];
        for (int j = i + 1; j < displays.count; j++)
        {
            Sprite *sp = [displays objectAtIndex:j];
            if ([s isIntersectsWithSprite:sp])
            {
                [s addIntersector:sp];
                [sp addIntersector:s];
            }
        }
    }
    
    for (s in displays)
    {
        if (s.intersectors.count)
        {
            [s triggerEvent:@"intersect"];
        }
    }

    [displays removeAllObjects];
    [self clearDepth];
    
    for (s in sprites)
    {
        if (s.display)
        {
            [displays addObject:s];
            [[depthIndex objectAtIndex:s.z] addObject: s];
        }

        [s clearIntersectors];
    }
}

- (void)show
{
    glClear(GL_COLOR_BUFFER_BIT);

    for (int z = 0; z < SceneDepth; z++)
    {
        NSMutableSet *se = [depthIndex objectAtIndex:z];
        for (Sprite *s in se)
        {
            if (s.textureInfo == nil) [s loadImage];
            if (s.visible) [s render:_effect];
        }
    }
}



@end