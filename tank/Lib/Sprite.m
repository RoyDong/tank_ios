//
//  Animation.m
//  sgg
//
//  Created by Roy on 7/19/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Scene.h"

bool CircleIntersectsRect(GLKVector2 center, float radius, CGRect rect)
{
    float w = fabsf(center.x - rect.origin.x);
    float h = fabsf(center.y - rect.origin.y);
    float halfWidth = rect.size.width / 2;
    float halfHeight = rect.size.height / 2;
    
    if (w >= radius + halfWidth) return NO;
    if (h >= radius + halfHeight) return NO;
    
    w -= halfWidth;
    h -= halfHeight;
    
    if (w * w + h * h >= radius * radius) return NO;
    
    return YES;
}

Vertex2D Vertex2DMake(GLfloat x, GLfloat y)
{
    Vertex2D v;
    v.x = x;
    v.y = y;
    
    return v;
}


@implementation Sprite
{
    Vertex2D vertices[4];
}

static const GLfloat texCoords[] = {
    0.0, 0.0,
    1.0, 0.0,
    0.0, 1.0,
    1.0, 1.0,
};

@synthesize sid = _sid;

@synthesize textureInfo = _textureInfo;

@synthesize radius = _radius;

@synthesize position = _position;

@synthesize rotation = _rotation;

@synthesize collisionType = _collisionType;

@synthesize intersectors = _intersectors;

@synthesize velocity = _velocity;

@synthesize rotateSpeed = _rotateSpeed;

@synthesize events = _events;

@synthesize display = _display;

@synthesize scene = _scene;

- (Sprite *)init
{
    self = [super init];
    
    if (self)
    {
        _intersectors = [[NSMutableSet alloc] init];
        _events = [[NSMutableDictionary alloc] init];
        self.visible = YES;
    }
    
    return self;
}

- (void)loadImage
{
    NSError *e;
    NSString *path = [[NSBundle mainBundle] pathForResource:self.image ofType:nil];
    NSDictionary *option = [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithBool:YES],
                            GLKTextureLoaderOriginBottomLeft, nil];

    _textureInfo = [GLKTextureLoader textureWithContentsOfFile:path options:option error:&e];
    
    if (_textureInfo == nil)
    {
        NSLog(@"Error loading file: %@", [e localizedDescription]);
        return;
    }

    vertices[1] = Vertex2DMake(0.0, 0.0);
    vertices[0] = Vertex2DMake(_textureInfo.width, 0.0);
    vertices[3] = Vertex2DMake(0.0, _textureInfo.height);
    vertices[2] = Vertex2DMake(_textureInfo.width, _textureInfo.height);
}

- (void)initPropertiesWithDict:(NSDictionary *)dict
{
    self.tag = [[dict objectForKey:@"tag"] intValue];
    self.visible = [[dict objectForKey:@"visible"] boolValue];
    _collisionType = [[dict objectForKey:@"collision_type"] intValue];
    _sid = [[dict objectForKey:@"id"] intValue];
    NSDictionary *v = [dict objectForKey:@"velocity"];
    _velocity = GLKVector2Make([[v objectForKey:@"x"] floatValue], [[v objectForKey:@"y"] floatValue]);
    v = [dict objectForKey:@"position"];
    _position = GLKVector2Make([[v objectForKey:@"x"] floatValue], [[v objectForKey:@"y"] floatValue]);
    _radius = [[dict objectForKey:@"radius"] floatValue];
    _rotateSpeed = [[dict objectForKey:@"rotate_speed"] floatValue];
    _rotation = [[dict objectForKey:@"rotation"] floatValue];
    self.display = [[dict objectForKey:@"display"] boolValue];
}

- (BOOL)isAtPosition:(GLKVector2)position
{
    return fabsf(self.position.x - position.x) <= 0.1 &&
        fabsf(self.position.y - position.y) <= 0.1;
}

- (GLKMatrix4)modelMatrix
{
    GLKMatrix4 m = GLKMatrix4Translate(GLKMatrix4Identity, self.position.x, self.position.y, 0);
    m = GLKMatrix4Rotate(m, self.rotation, 0, 0, 1);
    return GLKMatrix4Translate(m, - (float)(_textureInfo.width / 2), - (float)(_textureInfo.height / 2), 0);
}

- (void)render:(GLKBaseEffect *)effect
{
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    effect.texture2d0.name = _textureInfo.name;
    effect.texture2d0.enabled = YES;
    effect.transform.modelviewMatrix = [self modelMatrix];
    [effect prepareToDraw];

    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);

    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, texCoords);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(GLKVertexAttribPosition);
    glDisableVertexAttribArray(GLKVertexAttribTexCoord0);
    glDisable(GL_BLEND);
}

- (void)update:(float)dt
{
    _position = GLKVector2Add(_position, GLKVector2MultiplyScalar(_velocity, dt));

    if (_rotateSpeed > 0) _rotation += _rotateSpeed * dt;
}

- (void)clearIntersectors
{
    [_intersectors removeAllObjects];
}

- (void)addIntersector:(Sprite *)sprite
{
    [_intersectors addObject:sprite];
}

- (void)destroy
{
    if (_scene != nil)
    {
        [_scene removeSprite:self];
        _scene = nil;
    }
}

- (BOOL)isIntersectsWithSprite:(Sprite *)sprite
{
    if (self == sprite ||
        self.collisionType == CollisionTypeNone ||
        sprite.collisionType == CollisionTypeNone)
    {
        return NO;
    }

    if (self.collisionType == CollisionTypeRect)
    {
        if (sprite.collisionType == CollisionTypeRect)
            return CGRectIntersectsRect([self rect], [sprite rect]);
        
        if (sprite.collisionType == CollisionTypeCircle)
            return CircleIntersectsRect(sprite.position, sprite.radius, [self rect]);
    }

    if (self.collisionType == CollisionTypeCircle)
    {
        if (sprite.collisionType == CollisionTypeRect)
            return CircleIntersectsRect(self.position, self.radius, [sprite rect]);
        
        if (sprite.collisionType == CollisionTypeCircle)
            return GLKVector2Distance(self.position, sprite.position) < self.radius + sprite.radius;
    }

    return NO;
}

- (CGRect)rect
{
    return CGRectMake(_position.x, _position.y, _textureInfo.width, _textureInfo.height);
}

- (void)addHandler:(void (^)())handler onEvent:(NSString *)name
{
    NSMutableSet *handlers = [_events objectForKey:name];
    
    if (handlers == nil)
    {
        handlers = [[NSMutableSet alloc] init];
        [_events setObject:handlers forKey:name];
    }

    [handlers addObject:handler];
}

- (void)clearHandlersOnEvent:(NSString *)name
{
    [_events removeObjectForKey:name];
}

- (void)triggerEvent:(NSString *)name
{
    NSMutableSet *handlers = [_events objectForKey:name];
    
    if (handlers)
    {
        for (void (^handler)() in handlers) handler();
    }
}

@end
