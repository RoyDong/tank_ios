//
//  Cannon.m
//  sgg
//
//  Created by Roy on 7/24/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Cannon.h"
#import "MarkI.h"
#import "Scene.h"

@implementation Cannon
{
    NSDictionary *bombInfo;
}


@synthesize carrier = _carrier;

@synthesize chamber = _chamber;

@synthesize chamberSize = _chamberSize;

@synthesize reloadTime = _reloadTime;

@synthesize timeSinceLastReload = _timeSinceLastReload;


- (void)initPropertiesWithDict:(NSDictionary *)dict
{
    [super initPropertiesWithDict:dict];
    
    _chamberSize = [[dict objectForKey:@"chamber_size"] intValue];
    _reloadTime = [[dict objectForKey:@"reload_time"] floatValue];
    _timeSinceLastReload = [[dict objectForKey:@"time_since_last_reload"] floatValue];
    _chamber = [[NSMutableArray alloc] initWithCapacity:_chamberSize];
    bombInfo = [dict objectForKey:@"bomb"];
    for (int i = 0; i < _chamberSize; i++) [self reload];
}

- (void)fireAtPosition:(GLKVector2)position
{
    if (_chamber.count == 0)
    {
        [self triggerEvent:@"empty"];
        return;
    }
    
    float w = position.x - self.position.x;
    float h = position.y - self.position.y;
    float s = sqrtf(powf(w, 2) + powf(h, 2));
    float radian = acosf(w / s);
    if (h < 0) radian = 2 * M_PI - radian;
    self.rotation = radian;

    Bomb *bomb = [_chamber lastObject];
    float offset = bomb.radius + _carrier.radius;
    bomb.position = GLKVector2Make(self.position.x + offset * w / s, self.position.y + offset * h / s);
    bomb.rotation = self.rotation;
    [bomb flyByAngle:self.rotation];
    __weak Bomb *b = bomb;
    [bomb addHandler:^{
        bool bang = false;
        for (Sprite *s in b.intersectors)
        {
            if (s.class == MarkI.class && s.tag != b.tag)
            {
                bang = true;
            }
            else if (s.class == Bomb.class && s.tag != b.tag)
            {
                bang = true;
            }
        }
        if (bang) [b destroy];
    } onEvent:@"intersect"];

    [self.scene addSprite:bomb];
    [_chamber removeLastObject];
    [self triggerEvent:@"fire"];
}

- (void)reload
{
    if (_chamber.count < _chamberSize)
    {
        Bomb *bomb = [[Bomb alloc] init];
        bomb.image = @"bomb.png";
        bomb.launcher = self;
        bomb.radius = [[bombInfo objectForKey:@"radius"] floatValue];
        bomb.speed = [[bombInfo objectForKey:@"speed"] floatValue];
        bomb.timeout = [[bombInfo objectForKey:@"timeout"] floatValue];
        bomb.display = YES;
        bomb.collisionType = CollisionTypeCircle;
        [_chamber addObject:bomb];
    }
}

- (GLKVector2)position
{
    return _carrier ? _carrier.position : self.position;
}

- (void)update:(float)dt
{
    _timeSinceLastReload += dt;

    if (_timeSinceLastReload >= _reloadTime)
    {
        [self reload];
        _timeSinceLastReload = 0;
    }
}

- (void)destroy
{
    [_chamber removeAllObjects];
    [super destroy];
}

@end
