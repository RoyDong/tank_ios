//
//  MarkI.m
//  tank
//
//  Created by Roy on 8/18/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "MarkI.h"
#import "Scene.h"


@implementation MarkI
{
    float destRotation;
    float cos, sin, leg;
}

@synthesize direction = _direction;

@synthesize speed = _speed;

@synthesize status = _status;

@synthesize cannon = _cannon;


- (void)initPropertiesWithDict:(NSDictionary *)dict
{
    [super initPropertiesWithDict:dict];
    
    _cannon = [[Cannon alloc] init];
    [_cannon initPropertiesWithDict:[dict objectForKey:@"cannon"]];
    _cannon.image = @"bomb.png";
    _cannon.carrier = self;
    _cannon.z = self.z + 1;
    
    _speed = [[dict objectForKey:@"speed"] floatValue];
    NSDictionary *v = [dict objectForKey:@"direction"];
    self.direction = GLKVector2Make([[v objectForKey:@"x"] floatValue], [[v objectForKey:@"y"] floatValue]);
    _status = [[dict objectForKey:@"status"] intValue];
}

- (void)setDirection:(GLKVector2)direction
{
    leg = sqrtf(direction.x * direction.x + direction.y * direction.y);
    cos = direction.x / leg;
    sin = direction.y / leg;
    destRotation = direction.y < 0 ? 2 * M_PI - acosf(cos) : acosf(cos);
    _direction = direction;
    _status = 1;
}

- (void)update:(float)dt
{
    if (_status == 0 || _speed == 0) return;

    float r = destRotation - self.rotation;
    float dr = self.rotateSpeed * dt;
    float lastRotation = self.rotation;
    
    if (r < - M_PI)
    {
        self.rotation += dr;
        r = 2 * M_PI + r;
    }
    else if (r < 0)
    {
        self.rotation -= dr;
        r = -r;
    }
    else if (r <= M_PI)
    {
        self.rotation += dr;
    }
    else
    {
        r = 2 * M_PI - r;
        self.rotation -= dr;
    }

    if (dr > r || r < M_PI / 9 || self.rotateSpeed <= 0)
    {
        self.rotation = destRotation;
        self.position = GLKVector2Add(self.position, GLKVector2Make(dt * _speed * cos, dt * _speed * sin));
    }
    
    if (_cannon) _cannon.rotation += self.rotation - lastRotation;
}


- (void)fireAtPosition:(GLKVector2)position
{
    if (GLKVector2Distance(self.position, position) > self.radius)
        [_cannon fireAtPosition:position];
}

- (void)setScene:(Scene *)scene
{
    [super setScene:scene];
    [scene addSprite:_cannon];
}

- (void)setDisplay:(BOOL)display
{
    [_cannon setDisplay:display];
    [super setDisplay:display];
}

- (void)setVisible:(BOOL)visible
{
    [_cannon setVisible:visible];
    [super setVisible:visible];
}

- (void)destroy
{
    [_cannon destroy];
    _cannon = nil;
    [super destroy];
}

@end
