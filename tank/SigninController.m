//
//  SigninController.m
//  tank
//
//  Created by Roy on 8/24/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "SigninController.h"
#import "BattleController.h"

@interface SigninController ()

@end

@implementation SigninController
{
    UIActivityIndicatorView *spinner;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.view addSubview:spinner];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self buildEmailInputBox];
    [self buildPassInputBox];
    [self buildSigninBtn];
    [self buildOtherBtns];
}


- (void)buildEmailInputBox
{
    UIImageView *emailInputBack = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email_input.jpg"]];
    CGRect emailInputBackFrame = emailInputBack.frame;
    emailInputBackFrame.origin.x = 45.5;
    emailInputBackFrame.origin.y = 100;
    emailInputBack.frame = emailInputBackFrame;
    emailInputBack.userInteractionEnabled = YES;
    [self.view addSubview:emailInputBack];
    
    
    self.emailInput = [[UITextField alloc] initWithFrame:
                       CGRectMake(emailInputBackFrame.origin.x + 25,
                                  emailInputBackFrame.origin.y,
                                  emailInputBackFrame.size.width - 25,
                                  emailInputBackFrame.size.height)];
    self.emailInput.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.emailInput.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:self.emailInput];
}

- (void)buildPassInputBox
{
    UIImageView *passInputBack = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email_input.jpg"]];
    CGRect passInputBackFrame = passInputBack.frame;
    passInputBackFrame.origin.x = 45.5;
    passInputBackFrame.origin.y = 180;
    passInputBack.frame = passInputBackFrame;
    passInputBack.userInteractionEnabled = YES;
    [self.view addSubview:passInputBack];
    
    self.passInput = [[UITextField alloc] initWithFrame:
                      CGRectMake(passInputBackFrame.origin.x + 25,
                                 passInputBackFrame.origin.y,
                                 passInputBackFrame.size.width - 25,
                                 passInputBackFrame.size.height)];
    self.passInput.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.passInput.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:self.passInput];
}

- (void)buildSigninBtn
{
    UIImage *img = [UIImage imageNamed:@"btn_back.png"];
    self.signinBtn = [[UIButton alloc] initWithFrame:CGRectMake(185, 250, img.size.width, img.size.height)];
    [self.signinBtn setBackgroundImage:img forState:UIControlStateNormal];
    [self.signinBtn setTitle:@"登陆" forState:UIControlStateNormal];
    self.signinBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.signinBtn addTarget:self action:@selector(signin:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.signinBtn];
}

- (void)buildOtherBtns
{
    self.forgetPassBtn = [[UIButton alloc] initWithFrame:CGRectMake(45.5, 255, 50, 30)];
    [self.forgetPassBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
    [self.view addSubview:self.forgetPassBtn];
    self.forgetPassBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    self.signupBtn = [[UIButton alloc] initWithFrame:CGRectMake(110, 255, 50, 30)];
    [self.signupBtn setTitle:@"注册" forState:UIControlStateNormal];
    [self.view addSubview:self.signupBtn];
    self.signupBtn.titleLabel.font = [UIFont systemFontOfSize:12];
}

- (IBAction)signin:(UIButton *)sender
{
    [self.signinBtn setEnabled:false];
    CGSize size = self.view.bounds.size;
    [spinner setCenter:CGPointMake(size.width / 2, size.height / 2)];
    [spinner startAnimating];
    [self.emailInput resignFirstResponder];
    [self.passInput resignFirstResponder];

    TcpClient *client = [TcpClient singleInstance];
    [client callWithTitle:@"user.SigninOrSignup"
                             content:[NSString stringWithFormat:@"email=%@&passwd=%@", self.emailInput.text, self.passInput.text]
                            callback:^(NSDictionary *result){
                                [self.signinBtn setEnabled:true];
                                [spinner stopAnimating];
                                if ([[result objectForKey:@"code"] intValue] == 0)
                                {
                                    User *user = [[User alloc] initWithDict:[result objectForKey:@"data"]];
                                    [User setCurrent:user];
                                    BattleController *c = [[BattleController alloc] init];
                                    c.index = self.index;
                                    [c.index dismissViewControllerAnimated:true completion:^(){
                                        [c.index presentViewController:c animated:false completion:nil];
                                    }];
                                }
                                else
                                {
                                    [self.index popMessage:[result objectForKey:@"message"]];
                                }
                            }
     ];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailInput resignFirstResponder];
    [self.passInput resignFirstResponder];
}

@end
