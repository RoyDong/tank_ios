//
//  User.h
//  tank
//
//  Created by Roy on 8/6/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MarkI.h"

typedef enum {
    UserStateOffline  = 0,
    UserStateOnline   = 1,
    UserStateAfk      = 2,
    UserStateMatching = 3,
    UserStateMatched  = 4,
    UserStateReady    = 5,
    UserStateInBattle = 6,
} UserState;

@interface User : NSObject

@property (nonatomic) int uid;

@property (nonatomic, strong) NSString *email;

@property (nonatomic, strong) NSString *name;

@property (nonatomic) int rank;

@property (nonatomic) int tag;

@property (nonatomic) int tid;

@property (nonatomic) UserState state;

+ (User *)current;

+ (void)setCurrent:(User *)user;

- (User *)initWithDict:(NSDictionary *)dict;

@end