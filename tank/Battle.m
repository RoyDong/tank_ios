//
//  Battle.m
//  tank
//
//  Created by Roy on 8/6/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Battle.h"
#import "Tank.h"

@implementation Battle
{
    NSMutableDictionary *sprites;
    NSMutableArray *players;
}

@synthesize scene = _scene;

@synthesize creator = _creator;


static Battle * instance;

+ (Battle *)singleInstance
{
    return instance;
}

+ (void)setInstance:(Battle *)battle
{
    instance = battle;
}

- (Battle *)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    
    if (self)
    {
        sprites = [[NSMutableDictionary alloc] init];
        _scene = [[Scene alloc] init];
        self.name = [dict objectForKey:@"name"];
    }
    
    return self;
}


- (Sprite *)sprite:(int)sid
{
    return [_scene sprite:sid];
}

- (Sprite *)addSpriteWithData:(NSDictionary *)data
{
    NSString *type = [data objectForKey:@"sprite"];
    
    if ([type isEqualToString:@"tank"])
    {
        Tank *t = [[Tank alloc] init];
        t.image = @"round1.png";
        [t initPropertiesWithDict:data];
        [_scene addSprite:t];
        return t;
    }
    
    return nil;
}

- (void)syncSceneWithData:(NSDictionary *)data
{
    NSArray *storage = (NSArray *)[data objectForKey:@"sprites"];

    for (int i = 0; i < storage.count; i++)
    {
        NSDictionary *dict = [storage objectAtIndex:i];
        
        [self addSpriteWithData:dict];
    }
}

- (void)addPlayer:(User *)player
{
    [players addObject:player];
}


@end
