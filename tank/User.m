//
//  User.m
//  tank
//
//  Created by Roy on 8/6/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "User.h"

@implementation User


static User *instance;

+ (User *)current
{
    return instance;
}

+ (void)setCurrent:(User *)user
{
    instance = user;
}

- (User *)initWithDict:(NSDictionary *)dict
{
    self = [super self];
    
    if (self)
    {
        self.uid = [[dict objectForKey:@"id"] intValue];
        self.name = [dict objectForKey:@"name"];
        self.email = [dict objectForKey:@"email"];
        self.rank = [[dict objectForKey:@"rank"] intValue];
        self.tag = [[dict objectForKey:@"tag"] intValue];
        self.tid = [[dict objectForKey:@"tid"] intValue];
        self.state = [[dict objectForKey:@"state"] intValue];
    }
    
    return self;
}


@end