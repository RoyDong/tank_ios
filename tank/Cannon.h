//
//  Cannon.h
//  sgg
//
//  Created by Roy on 7/24/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Bomb.h"

@class MarkI;

@interface Cannon : Sprite

@property (nonatomic, weak) MarkI *carrier;

@property (nonatomic, strong) NSMutableArray *chamber;

@property (nonatomic) int chamberSize;

@property (nonatomic) float reloadTime;

@property (nonatomic, readonly) float timeSinceLastReload;

- (void)fireAtPosition:(GLKVector2)position;

@end