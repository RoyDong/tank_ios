//
//  IndexController.m
//  low_ios
//
//  Created by Roy on 7/30/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "SceneController.h"
#import "Scene.h"
#import "BattleController.h"
#import "TcpClient.h"


const int PathLength = 20;

const int ModeStop = 0;

const int ModePathStart = 1;

const int ModeCastStart = 2;

const int MoveMode = 3;


@implementation SceneController
{
    Scene *scene;
    TcpClient *client;
    User *user;
    GLKVector2 startPoint;
    int actionMode;
    int castIndex;
    int pathIndex;
    int moveIndex;
    
    CGPoint moveCameraPoint;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    user = [User current];
    self.view.bounds = [UIScreen mainScreen].bounds;
    GLKView *view = (GLKView *)self.view;
    scene = [[Scene alloc] init];
    [scene setView:view];
    client = [TcpClient singleInstance];

    __weak SceneController *__self = self;
    __weak Scene *__scene = scene;
    __weak User *__user = user;

    [client setHandler:^(NSDictionary *d){
        int sid = [[d objectForKey:@"id"] intValue];
        float x = [[d objectForKey:@"x"] floatValue];
        float y = [[d objectForKey:@"y"] floatValue];
        
        MarkI * t = (MarkI *)[__scene sprite:sid];
        t.direction = GLKVector2Make(x, y);
    }forNotice:@"move"];

    [client setHandler:^(NSDictionary *d){
        int sid = [[d objectForKey:@"id"] intValue];
        float x = [[d objectForKey:@"x"] floatValue];
        float y = [[d objectForKey:@"y"] floatValue];
        
        MarkI * t = (MarkI *)[__scene sprite:sid];
        [t fireAtPosition:GLKVector2Make(x, y)];
    }forNotice:@"fire"];
    
    [client setHandler:^(NSDictionary *d){
        for (id v in d)
        {
            [[__scene sprite:[v intValue]] setVisible:NO];
        }
    }forNotice:@"die"];
    
    
    [client setHandler:^(NSDictionary *d){
        for (id v in d)
        {
            MarkI *t = (MarkI *)[__scene sprite:[v intValue]];
            t.status = 0;
        }
    }forNotice:@"stop"];
    
    
    [client setHandler:^(NSDictionary *d){
        for (id v in d)
        {
            [[__scene sprite:[v intValue]] setVisible:YES];
        }
    }forNotice:@"respawn"];

    [client setHandler:^(NSDictionary *d){
            [__self addSpriteWithData:d];
    }forNotice:@"join"];
    
    [client setHandler:^(NSDictionary *d){
        NSArray *tags = (NSArray *)d;
        NSString *msg;
        if (__user.tag == [[tags objectAtIndex:0] intValue]) {
            msg = @"你输了";
        }
        else
        {
            msg = @"你赢了";
        }

        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"游戏结束"
                                                       message:msg
                                                      delegate:__self
                                             cancelButtonTitle:@"确定"
                                             otherButtonTitles:nil, nil];
        [alert show];
    }forNotice:@"battle_end"];


    [client callWithTitle:@"battle.Scene" content:nil callback:^(NSDictionary *dict){
        if ([[dict objectForKey:@"code"] intValue] == 0)
        {
            [__self syncSceneWithData:[dict objectForKey:@"data"]];
        }
        else
            [__self.index dismissViewControllerAnimated:NO completion:nil];
    }];
    

    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                     initWithTarget:self
                                     action:@selector(handleTap:)]];
    
    self.view.multipleTouchEnabled = YES;

    glClearColor(0.7, 0.7, 0.7, 1.0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    user.tid = 0;
    BattleController *c = [[BattleController alloc] init];
    c.index = self.index;
    [self.index dismissViewControllerAnimated:false completion:nil];
    [c.index presentViewController:c animated:false completion:nil];
}

- (void)syncSceneWithData:(NSDictionary *)data
{
    NSArray *storage = (NSArray *)[data objectForKey:@"sprites"];
    
    for (int i = 0; i < storage.count; i++)
    {
        NSDictionary *dict = [storage objectAtIndex:i];
        
        [self addSpriteWithData:dict];
    }
}

- (Sprite *)addSpriteWithData:(NSDictionary *)data
{
    NSString *type = [data objectForKey:@"sprite"];
    
    if ([type isEqualToString:@"tank"])
    {
        MarkI *t = [[MarkI alloc] init];
        t.image = @"round3.png";
        [t initPropertiesWithDict:data];
        [scene addSprite:t];
        return t;
    }
    
    return nil;
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    [scene show];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touches.count > 1)
    {
        float sumx = 0, sumy = 0;
        for (UITouch *touch in touches)
        {
            CGPoint point = [touch locationInView:self.view];
            sumx += point.x;
            sumy += point.y;
        }
        
        moveCameraPoint = CGPointMake(sumx / touches.count, sumy / touches.count);
        return;
    }

    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    startPoint = GLKVector2Make(point.x, scene.bounds.size.height - point.y);
    actionMode  = MoveMode;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touches.count > 1)
    {
        float sumx = 0, sumy = 0;
        for (UITouch *touch in touches)
        {
            CGPoint point = [touch locationInView:self.view];
            sumx += point.x;
            sumy += point.y;
        }
        
        CGPoint point = CGPointMake(sumx / touches.count, sumy / touches.count);
        CGPoint vector = CGPointMake(moveCameraPoint.x - point.x, point.y - moveCameraPoint.y);
        moveCameraPoint = point;
        [scene moveCameraByVector:vector];
        return;
    }

    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    GLKVector2 v = GLKVector2Make(point.x, scene.bounds.size.height - point.y);
        
    if (actionMode == MoveMode && GLKVector2Distance(v, startPoint) > 3 * 13)
    {
        [client callWithTitle:@"tank.Move" content:[NSString stringWithFormat:@"x=%f&y=%f", v.x - startPoint.x, v.y - startPoint.y] callback:nil];
        startPoint = v;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touches.count > 1)
    {
        return;
    }
    
    [client callWithTitle:@"tank.Stop" content:nil callback:nil];
    actionMode = ModeStop;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touches.count > 1)
    {
        return;
    }
    [client callWithTitle:@"tank.Stop" content:nil callback:nil];
    actionMode = ModeStop;
}

- (void)update
{
    [scene perform:self.timeSinceLastUpdate];
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:recognizer.view];
    if (point.x <= 30 && point.y <= 30)
    {
        [client callWithTitle:@"battle.Quit" content:nil callback:^(NSDictionary *data){
            user.tid = 0;
            BattleController *c = [[BattleController alloc] init];
            c.index = self.index;
            [self.index dismissViewControllerAnimated:NO completion:nil];
            [c.index presentViewController:c animated:false completion:nil];
        }];
        return;
    }
    
    GLKVector2 v = GLKVector2Make(point.x, scene.bounds.size.height - point.y);
    [client callWithTitle:@"tank.Fire" content:[NSString stringWithFormat:@"x=%f&y=%f", v.x, v.y] callback:nil];
}


@end