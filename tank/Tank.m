//
//  Tank.m
//  sgg
//
//  Created by Roy on 7/25/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import "Tank.h"
#import "Scene.h"


@implementation Tank

@synthesize destPosition = _destPosition;

@synthesize speed = _speed;

@synthesize status = _status;

@synthesize cannon = _cannon;


- (void)initPropertiesWithDict:(NSDictionary *)dict
{
    [super initPropertiesWithDict:dict];

    _cannon = [[Cannon alloc] init];
    [_cannon initPropertiesWithDict:[dict objectForKey:@"cannon"]];
    _cannon.image = @"bomb.png";
    //_cannon.carrier = self;
    _cannon.z = self.z + 1;
    
    _speed = [[dict objectForKey:@"speed"] floatValue];
    NSDictionary *v = [dict objectForKey:@"dest_position"];
    _status = 1;
    self.destPosition = GLKVector2Make([[v objectForKey:@"x"] floatValue], [[v objectForKey:@"y"] floatValue]);
}


- (void)update:(float)dt
{
    if (_status == 0 || _speed == 0 || [self isAtPosition:_destPosition]) return;

    float w = _destPosition.x - self.position.x;
    float h = _destPosition.y - self.position.y;
    float s = sqrtf(powf(w, 2) + powf(h, 2));
    float ds = _speed * dt;
    
    if (s <= ds)
    {
        self.position = _destPosition;
        [self triggerEvent:@"arrive"];
    }
    else
    {
        float cos = w / s;
        float destRadian = acosf(cos);
        if (h < 0) destRadian = 2 * M_PI - destRadian;
        if (_cannon) _cannon.rotation += destRadian - self.rotation;
        self.rotation = destRadian;
        self.position = GLKVector2Make(self.position.x + ds * cos, self.position.y + ds * h / s);
    }
}


- (void)fireAtPosition:(GLKVector2)position
{
    if (GLKVector2Distance(self.position, position) > self.radius)
        [_cannon fireAtPosition:position];
}

- (void)setScene:(Scene *)scene
{
    [super setScene:scene];
    [scene addSprite:_cannon];
}

- (void)setDisplay:(BOOL)display
{
    [_cannon setDisplay:display];
    [super setDisplay:display];
}

- (void)setVisible:(BOOL)visible
{
    [_cannon setVisible:visible];
    [super setVisible:visible];
}

- (void)destroy
{
    [_cannon destroy];
    _cannon = nil;
    [super destroy];
}
@end