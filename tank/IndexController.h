//
//  IndexController.h
//  tank
//
//  Created by Roy on 7/31/13.
//  Copyright (c) 2013 Roy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "TcpClient.h"

@interface IndexController : UIViewController

- (void)popMessage:(NSString *)text;

@end
